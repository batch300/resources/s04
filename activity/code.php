<?php 

class Building {

	protected $name;
	protected $floors;
	protected $address;

	public function __construct($name,$floors,$address){

		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;
	}

	public function getName(){

		return $this->name;
	}

	public function getFloors(){
		return $this->floors;
	}

	private function setFloors($floors){
		return $this->floors = $floors;
	}

	public function getAddress(){

		return $this->address;
	}

	private function setAddress($address){

		return $this->address = $address;
	}

	public function setName($name){

		return $this->name = $name;
	}
}

class Condominium extends Building{

  
}

$building = new Building('Caswyn Building',8,'Timog Avenue, Quezon City, Philippines');
$condominium = new Condominium('Enzo Condo',5,'Buendia Avenue, Makati City, Philippines');


/*<!-- DEBUG THE CODES SO THAT YOU MAY VIEW THE BOTH THE RETRIEVED TASKS IN GET AND POST WITH ITS CORRESPONDING OPTION VALUE/INDEX, YOU MAY REFER TO THE EXPECTED OUTPUT IN THE HANGOUTS -->

<!-- Paste this to an index.php file  -->*/


	$tasks = ["Get Git", "Bake HTML", "Eat CSS", "Learn PHP"];

	if(isset($_GET['variable']["i"])){
		$indexGet = $_GET["i"];
		"The retrieved task from GET is $tasks[$indeGet]. <b>";
	}

	if(isset($_POST["index"])){
		$indexPost = $_POST["index"];
		echo "The retrieved task from POST is $tasks[$indexPost]. <br>";
	}
