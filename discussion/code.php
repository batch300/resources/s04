<?php

class Building {

	//Access Modifiers
		//These are keywords that can be used to control the "visibility" of properties and methods in a class

		//1. public: fully open, properties or methods can be accessed from everywhere
		//2. private: properties or methods can only be accessed within the class and disables INHERITANCE
		//3. protected: properties and methods is only accessible within the class and on its child class


	protected $name;
	protected $floors;
	protected $address;

	public function __construct($name,$floors,$address){

		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;

	}

}


class Condominium extends Building{

	//These functions serves as the intermediary in accessing the object's property
	//These functions therefore defines whether an object's property can be accessed or changed
	//Getters and Setters
		//These are used to implement the encapsulation of an object's data
		//These are used to retrieve and modify values of each property of the object
		//Each property of an object should have a set of getters and setters

	//Encapsulation - indicates that data must not be directly accessible to users, but can be accessed only through public functions (setter and getter functions)

	//Getters and Setters

	//Getter - accessors
		//this is used to retrieve/access the value of an instantiated object
	public function getName(){
		return $this->name;
	}

	//Setter - mutators
		//this is used to change the default value of a property of an instantiated object
		//setter functions can also be modified to add data validations
	public function setName($name){

		if(gettype($name) === "string"){
			$this->name = $name;
		}
	}
}
$building = new Building('Caswynn Building',8,'Timog Avenue, Quezon City, Philippines');
$condominium = new Condominium('Enzo Condo',5,'Buendia Avenue, Makati City, Philippines');


//Mini-Activity (Pass by 7:40PM, Send a screencap of your output)
//Create a player class with these properties and methods
//Properties
	//protected - access modifiers
	//username 
	//email
	//password
	//level
	//guild
//Methods
	//attack
	//defend

//Create a Mage Class that is derived from the player class
//Create a getter and setter to change and retrieve the username and guild of the mage

//Instantiate three Mage objects/instances
//Use echo and the getter and setter functions

class Player {

	protected $username;
	protected $email;
	protected $password;
	protected $level;
	protected $guild;

	public function __construct($username, $email, $password,$level,$guild) {
	    $this->username = $username;
	    $this->email = $email;
	    $this->password = $password;
	    $this->level = $level;
	    $this->guild = $guild;
	}

	public function attack($enemy) {
		return "$this->username attacked $enemy";
	}

	public function defend(){
		return "$this->username defended itself!";
	}

}

class Mage extends Player{

	public function getUsername() {
        return $this->username;
    }

    public function setUsername($username) {
        $this->username = $username;
    }

	public function getGuild() {
        return $this->guild;
    }

    public function setGuild($guild) {
        $this->guild = $guild;
    }

}



$mage1 = new Mage('Lulu','luluyordle@mail.com','12345',45,'Yordle Guild');
$mage2 = new Mage('Draven','luluyordle@mail.com','12345',45,'Yordle Guild');