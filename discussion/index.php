<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S04: Access Modifiers and Inheritance</title>
</head>
<body>
	<h1>Access Modifiers</h1>
	<h2>Building Variables</h2>
	<!-- <p><?php //echo $building->name;?></p> -->
	<h2>Condominium Variables</h2>
	<!-- <p><?php //echo $condominium->name;?></p> -->

	<h1>Encapsulation</h1>
	<p>The name of the condominium is <?php echo $condominium->getName();?>.</p>

	<?php $condominium->setName(300)?>

	<p>The name of the condominium has been changed to <?php echo $condominium->getName(); ?></p>
	
	<p>Player: <?php echo $mage1->getUsername();?> of <?php echo $mage1->getGuild();?>.</p>
	<p><?php echo $mage1->attack($mage2->getUsername()); ?>!</p>
	<p>She changed her guild to: <?php echo $mage1->setGuild("Noxus Guild");?><?php echo $mage1->getGuild();?>!</p>


</body>
</html>